var theToggle = document.getElementById('toggle-menu');

// hasClass
function hasClass(elem, className) {
    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}

// addClass
function addClass(elem, className) {
    if (!hasClass(elem, className)) {
        elem.className += ' ' + className;
    }
}

// removeClass
function removeClass(elem, className) {
    var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
            newClass = newClass.replace(' ' + className + ' ', ' ');
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    }
}

// toggleClass
function toggleClass(elem, className) {
    var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(" " + className + " ") >= 0 ) {
            newClass = newClass.replace( " " + className + " " , " " );
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        elem.className += className;
    }
}

theToggle.onclick = function() {
    toggleClass(this, 'on');
    return false;
}

var slider = document.getElementById('slider-content');
var slides = document.getElementsByClassName("slide");
var dots = document.getElementsByClassName("dot");
var slideText = document.getElementsByClassName('slide-text');
var sliderControls = document.getElementById('slider-controls');
var slideIndex = 0;

showSlides(slideIndex);

function plusSlides(value) {
    showSlides(slideIndex += value);
}

function showSlideById(id) {
    showSlides(slideIndex = id);
}

function showSlides(value) {
    var i;
    if (value == slides.length) {
        slideIndex = 0;
    }
    if (value < 0) {
        slideIndex = slides.length - 1;
    }

    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }

    slides[slideIndex].style.display = "block";
    dots[slideIndex].className += " active";

    return slideIndex;
}

var sliderTimerId = setInterval(function() {
    slideIndex++;
    showSlides(slideIndex);
}, 3000);

function move(elem, t, f, dx) {
    var time = t;
    var fps = f;
    var steps = time / fps;
    var currentX = parseFloat(getComputedStyle(elem).left);
    var delta = (dx - currentX) / steps;
    var refresh = setInterval(function() {
        currentX += delta;
        elem.style.left = currentX.toFixed(10) + "px";
        steps--;
        if (steps == 0) {
            clearInterval(refresh);
        }
    }, (1000 / fps));
}

slider.onmouseenter = function() {
    clearInterval(sliderTimerId);
    for (i = 0; i < slides.length; i++) {
        if(getComputedStyle(slides[i]).display == "block") {
            if(parseFloat(getComputedStyle(slideText[i]).left) != 0) {
                move(slideText[i], 500, 50, 0);
            }
        } else {
            if(parseFloat(getComputedStyle(slideText[i]).left) != 0) {
                slideText[i].style.left = 0;
            }
        }
    }
    move(sliderControls, 500, 50, 0);
}

slider.onmouseleave = function() {
    for (i = 0; i < slides.length; i++) {
        if(getComputedStyle(slides[i]).display == "block") {
            move(slideText[i], 500, 50, -320);
        } else {
            slideText[i].style.left = -320 + "px";
        }
    }
    move(sliderControls, 500, 50, -320);

    sliderTimerId = setInterval(function() {
        slideIndex++;
        showSlides(slideIndex);
    }, 3000);
}
